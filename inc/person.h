#ifndef PERSON
#define PERSON
#include <cstdlib>
#include <string>
class Person {
public:
	Person(std::string name) :
		_next(NULL),
		_partner(NULL),
		_children(NULL),
		_childrenLen(0),
		_mother(NULL),
		_father(NULL),
		_isMale(false),
		_name(name),
		_birthDD(-1),
		_birthMM(-1),
		_birthYYYY(-1),
		_deathDD(-1),
		_deathMM(-1),
		_deathYYYY(-1)
		{}
	~Person()
	{
		free(_children);
	}
	Person *getNext() {return _next;}
	void setNext(Person *next) {_next = next;}

	Person *getPartner() {return _partner;}
	void setPartner(Person *partner) {_partner = partner;}

	Person **getChildren() {return _children;}
	void setChildren(Person **children) {_children = children;}
	size_t *getChildrenLen() {return &_childrenLen;}
	void setChildrenLen(size_t childrenLen) {_childrenLen = childrenLen;}

	Person *getMother() {return _mother;}
	void setMother(Person *mother) {_mother = mother;}

	Person *getFather() {return _father;}
	void setFather(Person *father) {_father = father;}

	bool getGender() {return _isMale;}
	void setGender(bool isMale) {_isMale = isMale;}

	std::string getName() {return _name;}
	void setName(std::string &name) {_name = name;}

	unsigned short int getBirthdateDD() {return _birthDD;}
	unsigned short int getBirthdateMM() {return _birthMM;}
	unsigned short int getBirthdateYYYY() {return _birthYYYY;}
	unsigned short int getDeathdateDD() {return _deathDD;}
	unsigned short int getDeathdateMM() {return _deathMM;}
	unsigned short int getDeathdateYYYY() {return _deathYYYY;}
	void setBirthdateDD(unsigned short dd) {_birthDD = dd;}
	void setBirthdateMM(unsigned short mm) {_birthMM = mm;}
	void setBirthdateYYYY(unsigned short yyyy) {_birthYYYY = yyyy;}
	void setDeathdateDD(unsigned short dd) {_deathDD = dd;}
	void setDeathdateMM(unsigned short mm) {_deathMM = mm;}
	void setDeathdateYYYY(unsigned short yyyy) {_deathYYYY = yyyy;}
private:
	Person *_next;
	Person *_partner;
	Person **_children;
	size_t _childrenLen;
	Person *_mother;
	Person *_father;
	bool _isMale;
	std::string _name;
	unsigned short int _birthDD;
	unsigned short int _birthMM;
	unsigned short int _birthYYYY;
	unsigned short int _deathDD;
	unsigned short int _deathMM;
	unsigned short int _deathYYYY;
};

Person *searchPersonByName(Person **, std::string &);

int AddPerson(Person **, size_t &, std::string &, Person **);

int AddRelMother(Person *, Person *);

int AddRelFather(Person *, Person *);

int AddRelCouple(Person *, Person *);

int AddRelChildToCouple(Person *, Person *, Person *);

int isBirthdateConsistent(Person *, unsigned short int, unsigned short int, unsigned short int, Person **);

int isDeathdateConsistent(Person *, unsigned short int, unsigned short int, unsigned short int);

void DeletePerson(Person *, Person **, size_t &);

bool IsValid(Person **, size_t &);

#endif
