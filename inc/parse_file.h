#ifndef PARSE_FILE
#define PARSE_FILE
#include "person.h"
#include <string>

void parseFile(std::string &, Person **, size_t &);

#endif
