# luca_peafowl

A tree dependency assignment so that Luca can act as a Peafowl

# How to compile and execute

1. Please create a build folder (that's the reason of entry in .gitignore btw)
2. Enter build folder like `cd build`
3. Issue `cmake ..`
4. Type `make`
5. Launch `./luca-peafowl`
