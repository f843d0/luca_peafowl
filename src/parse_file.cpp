#include "parse_file.h"
#include <iostream>
#include <sstream>
#include <fstream>

static void parseLine(std::string &line, size_t &linenumber, Person **personsPtr, size_t &personsLen)
{
	std::istringstream iss(line);
	std::string name;
	Person *personPtr;
	char instructionType;
	iss >> instructionType;
	switch (instructionType) {
	case 'P': {
		std::string date;
		std::istringstream tokener;
		std::istringstream stringToNumberConverter;
		std::string token;
		unsigned short int dd;
		unsigned short int mm;
		unsigned short int yyyy;
		Person *problemPtr;
		char gender;
		iss >> name;
		switch (AddPerson(personsPtr, personsLen, name, &personPtr)) {
		case 0:
			break;
		case -1:
			std::cout << "Person on line " << linenumber << " already exists, no action will be performed" << std::endl;
			return;
			break;
		case -2:
			std::cout << "Memory allocation failed for line " << linenumber << std::endl;
			return;
			break;
		default:
			std::cout << "Unknown error on line " << linenumber << std::endl;
			return;
			break;
		}
		iss >> gender;
		switch (gender) {
		case 'M':
			personPtr->setGender(true);
			break;
		case 'F':
			personPtr->setGender(false);
			break;
		default:
			std::cout << "Unknown gender on line " << linenumber << std::endl;
			return;
			break;
		}
		iss >> date;
		if (date.compare("-") != 0) {
			tokener.clear();
			tokener.str(date);
			std::getline(tokener, token, '/');
			stringToNumberConverter.clear();
			stringToNumberConverter.str(token);
			stringToNumberConverter >> dd;
			if (dd > 31)
				dd = 0;
			std::getline(tokener, token, '/');
			stringToNumberConverter.clear();
			stringToNumberConverter.str(token);
			stringToNumberConverter >> mm;
			if (mm > 12)
				mm = 0;
			std::getline(tokener, token, '/');
			stringToNumberConverter.clear();
			stringToNumberConverter.str(token);
			stringToNumberConverter >> yyyy;
			if (dd * mm * yyyy == 0) {
				if (!dd)
					std::cout << "Invalid day in Birthdate line " << linenumber << std::endl;
				else if (!mm)
					std::cout << "Invalid month in Birthdate line " << linenumber << std::endl;
				else
					std::cout << "Invalid year in Birthdate line" << linenumber << std::endl;
				return;
			}
			if (int ret = isBirthdateConsistent(personPtr, dd, mm, yyyy, &problemPtr)) {
				switch (ret) {
				case -1:
					std::cout << "Own Birthdate happens after Deathdate on line " << linenumber << std::endl;
					break;
				case -2:
					std::cout << "This person would be born after descendant " << problemPtr->getName() << " on line " << linenumber << std::endl;
					break;
				case -3:
					std::cout << "This person would be born before its Mother on line " << linenumber << std::endl;
					break;
				case -4:
					std::cout << "This person would be born before its Father on line " << linenumber << std::endl;
					break;
				default:
					std::cout << "Unknown Error on Birthdate on line " << linenumber << std::endl;
					break;
				}
				return;
			}
			personPtr->setBirthdateDD(dd);
			personPtr->setBirthdateMM(mm);
			personPtr->setBirthdateYYYY(yyyy);
		}
		iss >> date;
		if (date.compare("-") != 0) {
			tokener.clear();
			tokener.str(date);
			std::getline(tokener, token, '/');
			stringToNumberConverter.clear();
			stringToNumberConverter.str(token);
			stringToNumberConverter >> dd;
			if (dd > 31)
				dd = 0;
			std::getline(tokener, token, '/');
			stringToNumberConverter.clear();
			stringToNumberConverter.str(token);
			stringToNumberConverter >> mm;
			if (mm > 12)
				mm = 0;
			std::getline(tokener, token, '/');
			stringToNumberConverter.clear();
			stringToNumberConverter.str(token);
			stringToNumberConverter >> yyyy;
			if (dd * mm * yyyy == 0) {
				if (!dd)
					std::cout << "Invalid day in Deathdate line " << linenumber << std::endl;
				else if (!mm)
					std::cout << "Invalid month in Deathdate line " << linenumber << std::endl;
				else
					std::cout << "Invalid year in Deathdate line" << linenumber << std::endl;
				return;
			}
			if (int ret = isDeathdateConsistent(personPtr, dd, mm, yyyy)) {
				switch (ret) {
				case -1:
					std::cout << "Deathdate would happen before Birthdate on line " << linenumber << std::endl;
					break;
				case -2:
					std::cout << "Birthdate hasn't been set, won't apply Deathdate on line " << linenumber << std::endl;
					break;
				default:
					std::cout << "Unkown Error on Deathdate on line " << linenumber << std::endl;
					break;
				}
				return;
			}
			personPtr->setDeathdateDD(dd);
			personPtr->setDeathdateMM(mm);
			personPtr->setDeathdateYYYY(yyyy);
		}
		break;
	}
	case 'R': {
		Person *objectOfRelation;
		char relType;
		iss >> name;
		if (!(personPtr = searchPersonByName(personsPtr, name))) {
			std::cout << "Person in relation on line " << linenumber << " doesn't exist, no action will be taken" << std::endl;
			return;
		}
		iss >> relType;
		iss >> name;
		if (!(objectOfRelation = searchPersonByName(personsPtr, name))) {
			std::cout << "Person object of relation on line " << linenumber << " doesn't exist, no action will be taken" << std::endl;
			return;
		}
		switch (relType) {
		case 'C':
			switch (AddRelCouple(personPtr, objectOfRelation)) {
			case -1:
				std::cout << "First partner already has one on line " << linenumber << std::endl;
				break;
			case -2:
				std::cout << "Second partner already has one on line " << linenumber << std::endl;
				break;
			case -3:
				std::cout << "Partners are biologically incompatible on line " << linenumber << std::endl;
				break;
			}
			break;
		case 'M':
			switch (AddRelMother(personPtr, objectOfRelation)) {
			case -1:
				std::cout << "Specified Mother doesn't exist on line " << linenumber << std::endl;
				break;
			case -2:
				std::cout << "Specified Mother has wrong gender on line " << linenumber << std::endl;
				break;
			case -3:
				std::cout << "Memory allocation failed for line " << linenumber << std::endl;
				break;
			case -4:
				std::cout << "Mother already exists for line " << linenumber << std::endl;
				break;
			}
			break;
		case 'F':
			switch (AddRelFather(personPtr, objectOfRelation)) {
			case -1:
				std::cout << "Specified Father doesn't exist on line " << linenumber << std::endl;
				break;
			case -2:
				std::cout << "Specified Father has wrong gender on line " << linenumber << std::endl;
				break;
			case -3:
				std::cout << "Memory allocation failed for line " << linenumber << std::endl;
				break;
			case -4:
				std::cout << "Father already exists for line " << linenumber << std::endl;
				break;

			}
			break;
		default:
			std::cout << "Unknown relation on line " << linenumber << std::endl;
			break;
		}
		break;
	}
	default:
		std::cout << "Unknown instruction type on line " << linenumber << std::endl;
		break;
	}
	return;
}

void parseFile(std::string &filename, Person **personsPtr, size_t &personsLen)
{
	std::ifstream ifs(filename);
	std::string line;
	size_t lineNumber = 0;
	if (!ifs.is_open()) {
		std::cout << "Failed to open file" << std::endl;
		return;
	}
	while (std::getline(ifs, line)) {
		++lineNumber;
		parseLine(line, lineNumber, personsPtr, personsLen);
	}
	ifs.close();
	return;
}
