#include "person.h"

Person *searchPersonByName(Person **personsPtr, std::string &name)
{
	Person *it = *personsPtr;
	if (!it)
		return NULL;
	do
		if (name.compare(it->getName()) == 0)
			return it;
	while((it = it->getNext()));
	return NULL;
}

int AddPerson(Person **personsPtr, size_t &personsLen, std::string &name, Person **result)
{
	*result = NULL;
	if (searchPersonByName(personsPtr, name))
		return -1;
	Person *personPtr = (Person *)calloc(1, sizeof(Person));
	if (!personPtr)
		return -2;
	++personsLen;
	personPtr->setName(name);
	personPtr->setNext(NULL);
	*result = personPtr;
	if (personsLen == 1)
		*personsPtr = personPtr;
	else
		for (Person *it = *personsPtr; ; it = it->getNext())
			if (!it->getNext()) {
				it->setNext(personPtr);
				break;
			}
	return 0;
}

static int addChildToPerson(Person *parentPtr, Person *child)
{
	Person **children = parentPtr->getChildren();
	for (size_t i = 0; i < *(parentPtr->getChildrenLen()); ++i)
		if (children[i] == child)
			return 0;
	void *rea = realloc(children, sizeof(Person *) * ++(*(parentPtr->getChildrenLen())));
	if (!rea)
		return -3;
	parentPtr->setChildren((Person **)rea);
	parentPtr->getChildren()[*(parentPtr->getChildrenLen()) - 1] = child;
	return 0;
}

static int addParent(Person *parentPtr, Person *personPtr, bool isMale)
{
	if (parentPtr->getGender() != isMale)
		return -2;
	if (isMale) {
		if (personPtr->getFather())
			return -4;
		personPtr->setFather(parentPtr);
	}
	else {
		if (personPtr->getMother())
			return -4;
		personPtr->setMother(parentPtr);
	}
	return addChildToPerson(parentPtr, personPtr);
}

int AddRelMother(Person *motherPtr, Person *personPtr)
{
	return addParent(motherPtr, personPtr, false);
}

int AddRelFather(Person *fatherPtr, Person *personPtr)
{
	return addParent(fatherPtr, personPtr, true);
}

int AddRelCouple(Person *partner1Ptr, Person *partner2Ptr)
{
	if (partner1Ptr->getPartner())
		return -1;
	if (partner2Ptr->getPartner())
		return -2;
	if (partner1Ptr->getGender() == partner2Ptr->getGender())
		return -3;
	partner1Ptr->setPartner(partner2Ptr);
	partner2Ptr->setPartner(partner1Ptr);
	return 0;
}

int AddRelChildToCouple(Person *partner1Ptr, Person *partner2Ptr, Person *child)
{
	if (partner1Ptr->getPartner() != partner2Ptr)
		return -1;
	if (addChildToPerson(partner1Ptr, child))
		return -2;
	if (addChildToPerson(partner2Ptr, child))
		return -3;
	return 0;
}

static int checkChildren(Person *personPtr, unsigned short int dd, unsigned short int mm, unsigned short int yyyy, Person **problemPtr)
{
	int ret = 0;
	if ( (personPtr->getBirthdateDD() && personPtr->getBirthdateMM() && personPtr->getBirthdateYYYY())
		&& ((unsigned long int)(yyyy * 372 + mm * 31 + dd) >= (unsigned long int)(personPtr->getBirthdateYYYY() * 372 + personPtr->getBirthdateMM() * 31 + personPtr->getBirthdateDD()))
		) {
		*problemPtr = personPtr;
		return -2;
	}
	for (size_t i = 0; i < *(personPtr->getChildrenLen()); ++i)
		if ((ret = checkChildren(personPtr->getChildren()[i], dd, mm, yyyy, problemPtr)))
			break;
	return ret;
}

int isBirthdateConsistent(Person *personPtr, unsigned short int dd, unsigned short int mm, unsigned short int yyyy, Person **problemPtr)
{
	if ( (personPtr->getDeathdateDD() && personPtr->getDeathdateMM() && personPtr->getDeathdateYYYY())
		&& ((unsigned long int)(yyyy * 372 + mm * 31 + dd) >= (unsigned long int)(personPtr->getDeathdateYYYY() * 372 + personPtr->getDeathdateMM() * 31 + personPtr->getDeathdateDD()))
		)
		return -1;
	/*
	 * TODO: following checks should be extended to all potential Mothers and Fathers recursively in a similar way to below function
	 */
	if ( personPtr->getMother()
		&& (personPtr->getMother()->getBirthdateDD() && personPtr->getMother()->getBirthdateMM() && personPtr->getMother()->getBirthdateYYYY())
		&& ((unsigned long int)(yyyy * 372 + mm * 31 + dd) <= (unsigned long int)(personPtr->getMother()->getBirthdateYYYY() * 372 + personPtr->getMother()->getBirthdateMM() * 31 + personPtr->getMother()->getBirthdateDD()))
		)
		return -3;
	if ( personPtr->getFather()
		&& (personPtr->getFather()->getBirthdateDD() && personPtr->getFather()->getBirthdateMM() && personPtr->getFather()->getBirthdateYYYY())
		&& ((unsigned long int)(yyyy * 372 + mm * 31 + dd) <= (unsigned long int)(personPtr->getFather()->getBirthdateYYYY() * 372 + personPtr->getFather()->getBirthdateMM() * 31 + personPtr->getFather()->getBirthdateDD()))
		)
		return -4;
	return checkChildren(personPtr, dd, mm, yyyy, problemPtr);
}

int isDeathdateConsistent(Person *personPtr, unsigned short int dd, unsigned short int mm, unsigned short int yyyy)
{
	if (!personPtr->getBirthdateDD() || !personPtr->getBirthdateMM() || !personPtr->getBirthdateYYYY())
		return -2;
	if ( ((unsigned long int)(yyyy * 372 + mm * 31 + dd) < (unsigned long int)(personPtr->getBirthdateYYYY() * 372 + personPtr->getBirthdateMM() * 31 + personPtr->getBirthdateDD()))
		)
		return -1;
	return 0;
}

static void deletePersonAndLegacy(Person **personPtr, Person **personsPtr, size_t &personsLen)
{
	/*
	 * Traverse linked list and search for who is having Person as next.
	 * Since Person is gonna be removed soon, we set next with Person's
	 * next, we "bridge" to next node simply speaking
	 */
	for (Person *it = *personsPtr; ; it = it->getNext()) {
		if (it->getNext() == *personPtr)
			it->setNext((*personPtr)->getNext());
		if (!it->getNext())
			break;
	}

	/*
	 * If Person has partner, remove relation and destroy the other's children
	 */
	if ((*personPtr)->getPartner()) {
		(*personPtr)->getPartner()->setPartner(NULL);
		free((*personPtr)->getPartner()->getChildren());
		(*personPtr)->getPartner()->setChildren(NULL);
		(*personPtr)->getPartner()->setChildrenLen(0);
	}
	/*
	 * Traverse the list of Person's children and apply this very same function recursively
	 */
	for (size_t i = 0; i < *((*personPtr)->getChildrenLen()); ++i)
		deletePersonAndLegacy(&((*personPtr)->getChildren()[i]), personsPtr, personsLen);

	/*
	 * Deallocate this Person and update personsLen
	 */
	if (*personPtr == *personsPtr)
		*personsPtr = (*personPtr)->getNext();
	free((*personPtr)->getChildren());
	free((*personPtr));
	*personPtr = NULL;
	--personsLen;
	return;
}

static void resizeParent(Person *parent, Person *entry)
{
	if (!parent)
		return;
	for (size_t i = 0; i < *(parent->getChildrenLen()); ++i)
		if (parent->getChildren()[i] == entry) {
			parent->getChildren()[i] = parent->getChildren()[*(parent->getChildrenLen()) - 1];
			void *re = realloc(parent->getChildren(), sizeof(Person *) * --(*(parent->getChildrenLen())));
			if (re)
				parent->setChildren((Person **)re);
			if (!*(parent->getChildrenLen())) {
				parent->setChildren(NULL);
			}
			return;
		}
}

void DeletePerson(Person *personPtr, Person **personsPtr, size_t &personsLen)
{
	Person *mother = personPtr->getMother();
	Person *father = personPtr->getFather();
	Person *personEntry = personPtr;
	deletePersonAndLegacy(&personPtr, personsPtr, personsLen);
	resizeParent(mother, personEntry);
	resizeParent(father, personEntry);
}

static bool isInArrayValidityTraverser(Person *entry, Person **arrayValidityTraverser, size_t &arrayValidityTraverserLen)
{
	for (size_t i = 0; i < arrayValidityTraverserLen; ++i)
		if (arrayValidityTraverser[i] == entry)
			return true;
	return false;
}

static void validityTraverseEmAll(Person *current, Person **arrayValidityTraverser, size_t &arrayValidityTraverserLen)
{
	if (!current)
		return;
	for (size_t i = 0; i < arrayValidityTraverserLen; ++i)
		if (arrayValidityTraverser[i] == current)
			arrayValidityTraverser[i] = arrayValidityTraverser[--arrayValidityTraverserLen];
	if (isInArrayValidityTraverser(current->getFather(), arrayValidityTraverser, arrayValidityTraverserLen))
		validityTraverseEmAll(current->getFather(), arrayValidityTraverser, arrayValidityTraverserLen);
	if (isInArrayValidityTraverser(current->getMother(), arrayValidityTraverser, arrayValidityTraverserLen))
		validityTraverseEmAll(current->getMother(), arrayValidityTraverser, arrayValidityTraverserLen);
	if (isInArrayValidityTraverser(current->getPartner(), arrayValidityTraverser, arrayValidityTraverserLen))
		validityTraverseEmAll(current->getPartner(), arrayValidityTraverser, arrayValidityTraverserLen);
	for (size_t i = 0; i < *(current->getChildrenLen()); ++i)
		if (isInArrayValidityTraverser(current->getChildren()[i], arrayValidityTraverser, arrayValidityTraverserLen))
			validityTraverseEmAll(current->getChildren()[i], arrayValidityTraverser, arrayValidityTraverserLen);
}

bool IsValid(Person **personsPtr, size_t &personsLen)
{
	/*
	 * Idea is to allocate an array of pointers to Person. At first we
	 * forcefully specify the first node of linked list. We search for any
	 * valid relation between Mother, Father, Partner and Children. We
	 * remove current entry from the array and we take the found relation
	 * above as input for the very same function (recursive). If the array
	 * survives with some length, then the System is disconnected.
	 */
	if (!*personsPtr)
		return true;
	size_t arrayValidityTraverserLen = personsLen;
	Person **arrayValidityTraverser = (Person **)calloc(arrayValidityTraverserLen, sizeof(Person *));
	size_t i = 0;
	for (Person *it = *personsPtr; ; it = it->getNext()) {
		arrayValidityTraverser[i] = it;
		++i;
		if (!it->getNext())
			break;
	}
	validityTraverseEmAll(arrayValidityTraverser[0], arrayValidityTraverser, arrayValidityTraverserLen);
	free(arrayValidityTraverser);
	if (arrayValidityTraverserLen)
		return false;
	return true;
}
