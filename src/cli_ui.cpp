#include "cli_ui.h"
#include "parse_file.h"
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <sstream>

#ifndef MAX_LEN_OUT
#define MAX_LEN_OUT 1024
#endif

static void clearScreen();
static void makeMenu(const char [][MAX_LEN_OUT], size_t); 

static const char entries[][MAX_LEN_OUT] = {
	"Load from file",
	"Add person",
	"Set Mother of",
	"Set Father of",
	"Set Couple between",
	"Add child to Couple",
	"Set Birthdate of",
	"Set Deathdate of",
	"Remove person",
	"Validity Check",
	"Text printout",
	"Experimental ASCII printout",
	"Quit"
};

static void loadFromFile(Person **personsPtr, size_t &personsLen)
{
	std::string filename;
	clearScreen();
	std::cout << "Please enter filename:";
	std::cin >> filename;
	parseFile(filename, personsPtr, personsLen);
	std::cout << "Press ENTER to continue";
	std::cin.ignore();
	std::cin.get();
	return;
}

static void addPerson(Person **personsPtr, size_t &personsLen)
{
	std::string strInput;
	Person *personPtr = NULL;
	bool isMale;
	clearScreen();
	std::cout << "Please enter person name:";
	std::cin >> strInput;
	if (int ret = AddPerson(personsPtr, personsLen, strInput, &personPtr)) {
		switch (ret) {
		case -1:
			std::cout << "This person already exists, getting back to main menu now" << std::endl;
			break;
		case -2:
			std::cout << "Memory allocation failed, this is bad" << std::endl;
			break;
		default:
			std::cout << "AddPerson returned unknown error" << std::endl;
			break;
		}
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	std::cout << "Please now specify 0 for Female or anything else for Male:";
	std::cin >> strInput;
	std::stringstream ss(strInput);
	ss >> isMale;
	if (personPtr)
		personPtr->setGender(isMale);
	return;
}

static void setParent(Person **personsPtr, size_t &personsLen, bool isMale)
{
	std::string person;
	std::string parent;
	std::string specialization = (isMale) ? "Father" : "Mother";
	Person *personPtr;
	Person *parentPtr;
	clearScreen();
	std::cout << "Please enter person name:";
	std::cin >> person;
	personPtr = searchPersonByName(personsPtr, person);
	if (!personPtr) {
		std::cout << "This person doesn't exist, cowardly quitting to main menu now" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	std::cout << "Please enter " << specialization << " name:";
	std::cin >> parent;
	parentPtr = searchPersonByName(personsPtr, parent);
	if (!parentPtr) {
		std::cout << "This " << specialization << " doesn't exist, cowardly quitting to main menu now" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	if (int ret = (isMale) ? AddRelFather(parentPtr, personPtr) : AddRelMother(parentPtr, personPtr)) {
		switch (ret) {
		case -2:
			std::cout << "Selected " << specialization << " has wrong gender, refusing to apply and quitting to main menu" << std::endl;
			break;
		case -3:
			std::cout << "Memory allocation failed, this is bad" << std::endl;
			break;
		case -4:
			std::cout << "There's already " << specialization << " set, refusing to apply" << std::endl;
			break;
		default:
			std::cout << "AddRel" << specialization << " returned unknown error" << std::endl;
			break;
		}
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
	}
	return;
}

static void setMotherOf(Person **personsPtr, size_t &personsLen)
{
	setParent(personsPtr, personsLen, false);
}

static void setFatherOf(Person **personsPtr, size_t &personsLen)
{
	setParent(personsPtr, personsLen, true);
}

static void setCoupleBetween(Person **personsPtr, size_t &personsLen)
{
	std::string partner;
	Person *partner1 = NULL;
	Person *partner2 = NULL;
	clearScreen();
	std::cout << "Please enter name of first partner:";
	std::cin >> partner;
	if (!(partner1 = searchPersonByName(personsPtr, partner))) {
		std::cout << "This person doesn't exist, getting back to main menu now" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	std::cout << "Please enter name of second partner:";
	std::cin >> partner;
	if (!(partner2 = searchPersonByName(personsPtr, partner))) {
		std::cout << "This person doesn't exist, getting back to main menu now" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	if (int ret = AddRelCouple(partner1, partner2)) {
		switch (ret) {
		case -1:
			std::cout << "First partner already has one, refusing to apply" << std::endl;
			break;
		case -2:
			std::cout << "Second partner already has one, refusing to apply" << std::endl;
			break;
		case -3:
			std::cout << "Partners are biologically incompatible" << std::endl;
			break;
		default:
			std::cout << "Unknown Error" << std::endl;
			break;
		}
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
	}
	return;
}

static void addChildToCouple(Person **personsPtr, size_t &personsLen)
{
	Person *partner1 = NULL;
	Person *partner2 = NULL;
	Person *child = NULL;
	std::string strInput;
	clearScreen();
	std::cout << "Please insert first partner:";
	std::cin >> strInput;
	if (!(partner1 = searchPersonByName(personsPtr, strInput))) {
		std::cout << "This person doesn't exist, getting back to main menu now" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	std::cout << "Please insert second partner:";
	std::cin >> strInput;
	if (!(partner2 = searchPersonByName(personsPtr, strInput))) {
		std::cout << "This person doesn't exist, getting back to main menu now" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	std::cout << "Please insert child:";
	std::cin >> strInput;
	if (!(child = searchPersonByName(personsPtr, strInput))) {
		std::cout << "This person doesn't exist, getting back to main menu now" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	if (int ret = AddRelChildToCouple(partner1, partner2, child)) {
		switch (ret) {
		case -1:
			std::cout << "They are not partners! Refusing to proceed" << std::endl;
			break;
		case -2:
			std::cout << "Failed to add child to first partner" << std::endl;
			break;
		case -3:
			std::cout << "Failed to add child to second partner" << std::endl;
			break;
		default:
			std::cout << "Unknown Error" << std::endl;
			break;
		}
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
	}
	return;
}

static void acquireDate(Person **personsPtr, size_t &personsLen, bool isBirthdate)
{
	std::string strInput;
	std::istringstream tokener;
	std::istringstream stringToNumberConverter;
	std::string token;
	unsigned short int dd;
	unsigned short int mm;
	unsigned short int yyyy;
	Person *personPtr;
	Person *problemPtr;
	clearScreen();
	std::cout << "Please enter person name:";
	std::cin >> strInput;
	if (!(personPtr = searchPersonByName(personsPtr, strInput))) {
		std::cout << "This person doesn't exist, getting back to main menu now" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	std::cout << "Please enter DD/MM/YYYY:";
	std::cin >> strInput;
	tokener.clear();
	tokener.str(strInput);
	std::getline(tokener, token, '/');
	stringToNumberConverter.clear();
	stringToNumberConverter.str(token);
	stringToNumberConverter >> dd;
	/*
	 * Following is a potentially weak check: day of month can be 28, 30,
	 * 31 or 29 depending on the year. But this would involve quite a lot
	 * more of coding and hopefully it can be assumed that the real bug is
	 * the calendar currently in use: time should me expressed with radians
	 * of revolutions around the Sun, and most likely starting with 1666
	 * when Newton started Physics...
	 */
	if (dd > 31)
		dd = 0;
	std::getline(tokener, token, '/');
	stringToNumberConverter.clear();
	stringToNumberConverter.str(token);
	stringToNumberConverter >> mm;
	if (mm > 12)
		mm = 0;
	std::getline(tokener, token, '/');
	stringToNumberConverter.clear();
	stringToNumberConverter.str(token);
	stringToNumberConverter >> yyyy;
	if (dd * mm * yyyy == 0) {
		if (!dd)
			std::cout << "Invalid day" << std::endl;
		else if (!mm)
			std::cout << "Invalid month" << std::endl;
		else
			std::cout << "Invalid year" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	/*
	 * Now on validity of date wider than mere input. Assignment is stating
	 * that:
	 * - Birthdate of one person must not be older than its own Deathdate
	 * - Birthdate of one person must be older than that of all children and descendants
	 * - Deathdate of one person must not be earlier than its Birthdate
	 */
	if (isBirthdate) {
		if (int ret = isBirthdateConsistent(personPtr, dd, mm, yyyy, &problemPtr)) {
			switch (ret) {
			case -1:
				std::cout << "Own Birthdate happens after Deathdate, panic!" << std::endl;
				break;
			case -2:
				std::cout << "This person would be born after descendant " << problemPtr->getName() << std::endl;
				break;
			case -3:
				std::cout << "This person would be born before its Mother, panic!" << std::endl;
				break;
			case -4:
				std::cout << "This person would be born before its Father, panic!" << std::endl;
				break;
			default:
				std::cout << "Unknown Error" << std::endl;
				break;
			}
			std::cout << "Press ENTER to continue" << std::endl;
			std::cin.ignore();
			std::cin.get();
			return;
		}
		personPtr->setBirthdateDD(dd);
		personPtr->setBirthdateMM(mm);
		personPtr->setBirthdateYYYY(yyyy);
	}
	else {
		if (int ret = isDeathdateConsistent(personPtr, dd, mm, yyyy)) {
			switch (ret) {
			case -1:
				std::cout << "Own Deathdate happens before Birthdate, panic!" << std::endl;
				break;
			case -2:
				std::cout << "Birthdate hasn't been set, refusing to set Deathdate" << std::endl;
			default:
				std::cout << "Unknown Error" << std::endl;
				break;
			}
			std::cout << "Press ENTER to continue" << std::endl;
			std::cin.ignore();
			std::cin.get();
			return;
		}
		personPtr->setDeathdateDD(dd);
		personPtr->setDeathdateMM(mm);
		personPtr->setDeathdateYYYY(yyyy);
	}
	return;
}

static void setBirthdateOf(Person **personsPtr, size_t &personsLen)
{
	acquireDate(personsPtr, personsLen, true);
}

static void setDeathDateOf(Person **personsPtr, size_t &personsLen)
{
	acquireDate(personsPtr, personsLen, false);
}

static void removePerson(Person **personsPtr, size_t &personsLen)
{
	Person *personPtr;
	std::string name;
	clearScreen();
	std::cout << "Please enter person name:";
	std::cin >> name;
	if (!(personPtr = searchPersonByName(personsPtr, name))) {
		std::cout << "This person doesn't exist, getting back to main menu now" << std::endl;
		std::cout << "Press ENTER to continue" << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	DeletePerson(personPtr, personsPtr, personsLen);
	return;
}

static void validityCheck(Person **personsPtr, size_t &personsLen)
{
	clearScreen();
	if (IsValid(personsPtr, personsLen))
		std::cout << "System is valid" << std::endl;
	else
		std::cout << "System is NOT valid, it's disconnected" << std::endl;
	std::cin.ignore();
	std::cin.get();
	return;
}

static void textPrintout(Person **personsPtr, size_t &personsLen)
{
	/*
	 * Only printing sequentially for the time being, gotta improve later
	 */
	Person *it = *personsPtr;
	if (!it)
		return;
	clearScreen();
	do {
		std::cout << "Name: " << it->getName() << std::endl;
		std::cout << "Memory address: " << it << std::endl;
		std::cout << "Next: " << it->getNext() << std::endl;
		std::cout << "Partner: " << it->getPartner() << std::endl;
		std::cout << "Birthdate: " << it->getBirthdateDD() << "/" << it->getBirthdateMM() << "/" << it->getBirthdateYYYY() << std::endl;
		std::cout << "Deathdate: " << it->getDeathdateDD() << "/" << it->getDeathdateMM() << "/" << it->getDeathdateYYYY() << std::endl;
		std::cout << "Children: " << it->getChildren() << std::endl;
		std::cout << "Children Len: " << *(it->getChildrenLen()) << std::endl;
		for (size_t i = 0; i < *(it->getChildrenLen()); ++i)
			std::cout << "\t" << it->getChildren()[i] << std::endl;
		std::cout << "Mother: " << it->getMother() << std::endl;
		std::cout << "Father: " << it->getFather() << std::endl;
		std::cout << std::endl;
	} while((it = it->getNext()));
	std::cout << "Press ENTER to continue" << std::endl;
	std::cin.ignore();
	std::cin.get();
}

static void experimentalASCIIPrintOut(Person **personsPtr, size_t &personsLen)
{

}

static void quit(Person **personsPtr, size_t &personsLen)
{
	return;
}

void (*actions[sizeof(entries) / sizeof(entries[0])])(Person **, size_t &) = {
	quit,
	loadFromFile,
	addPerson,
	setMotherOf,
	setFatherOf,
	setCoupleBetween,
	addChildToCouple,
	setBirthdateOf,
	setDeathDateOf,
	removePerson,
	validityCheck,
	textPrintout,
	experimentalASCIIPrintOut
};


static void clearScreen()
{
	/*
	 * Should go for cls in Windoze
	 */
	system("clear");
}

static void makeMenu(const char menuEntries[][MAX_LEN_OUT], size_t len)
{
	for (size_t i = 0; i < len; ++i)
		std::cout << std::setw(std::to_string(len).length()) << (i + 1) * (i != len - 1) << ") " << menuEntries[i] << std::endl;

	std::cout << std::endl << "Please enter choice:";
}

int cliUI(Person **personsPtr, size_t &personsLen)
{
	std::string strEntry;
	unsigned long int ret;
	clearScreen();
	makeMenu(entries, sizeof(entries) / sizeof(entries[0]));
	std::cin >> strEntry;
	std::stringstream ss(strEntry);
	ss >> ret;
	if (ret < sizeof(entries) / sizeof(entries[0]))
		actions[ret](personsPtr, personsLen);
	return ret;
}

